--pass:12345678
INSERT INTO USER (ID,FIRST_NAME,LAST_NAME,USER_NAME,PASSWORD,ROLE,IS_ENABLED)
VALUES
(NEXTVAL('USER_SEQ'),'Alex','Ivanov','alex','$2a$10$l6b8TKDboRjGI0K0Hm.R/.euxnE67aAPgpqKgLBN9.74HQQ3vrol6','ROLE_USER',true);
--pass:123457
INSERT INTO USER (ID,FIRST_NAME,LAST_NAME,USER_NAME,PASSWORD,ROLE,IS_ENABLED)
VALUES
(NEXTVAL('USER_SEQ'),'Ivan','Petrov','ivan','$2a$10$9r68pxOs7BrCTk5k8apzxO/3E2VBRjZ3gb5DVjWxtBFBl9vsneErq','ROLE_USER',true);

--USER:1

INSERT INTO FLOW_MONITORING_HISTORY (ID,USER_ID,VALUE,FLOW_TYPE,CREATED_DATETIME)
VALUES
(NEXTVAL('FLOW_MONITORING_HISTORY_SEQ'),1, 10.56,'GAZ',PARSEDATETIME ('01-01-18 11:34:24','dd-MM-yy hh:mm:ss'));

INSERT INTO FLOW_MONITORING_HISTORY (ID,USER_ID,VALUE,FLOW_TYPE,CREATED_DATETIME)
VALUES
(NEXTVAL('FLOW_MONITORING_HISTORY_SEQ'),1, 10.57,'GAZ',PARSEDATETIME ('01-01-20 11:34:24','dd-MM-yy hh:mm:ss'));

INSERT INTO FLOW_MONITORING_HISTORY (ID,USER_ID,VALUE,FLOW_TYPE,CREATED_DATETIME)
VALUES
(NEXTVAL('FLOW_MONITORING_HISTORY_SEQ'),1, 10.58,'GAZ',PARSEDATETIME ('02-01-20 11:34:25','dd-MM-yy hh:mm:ss'));

--USER:2

INSERT INTO FLOW_MONITORING_HISTORY (ID,USER_ID,VALUE,FLOW_TYPE,CREATED_DATETIME)
VALUES
(NEXTVAL('FLOW_MONITORING_HISTORY_SEQ'),2, 100.75,'COLD_WATER',PARSEDATETIME ('31-01-19 11:35:24','dd-MM-yy hh:mm:ss'));

INSERT INTO FLOW_MONITORING_HISTORY (ID,USER_ID,VALUE,FLOW_TYPE,CREATED_DATETIME)
VALUES
(NEXTVAL('FLOW_MONITORING_HISTORY_SEQ'),2, 100.77,'COLD_WATER',PARSEDATETIME ('31-01-20 11:35:25','dd-MM-yy hh:mm:ss'));