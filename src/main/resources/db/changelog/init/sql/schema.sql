
create sequence USER_SEQ START WITH 1;

create sequence FLOW_MONITORING_HISTORY_SEQ START WITH 1;

create type flow AS ENUM ('GAZ', 'HOT_WATER','COLD_WATER');

create type role AS ENUM ('ROLE_USER','ACTUATOR');

create table FLOW_MONITORING_HISTORY
(
    ID NUMBER (10) not null
    constraint FLOW_MONITORING_HISTORY_PK
    primary key,
    USER_ID number (10),
    VALUE NUMERIC(8, 4),
    FLOW_TYPE flow,
    CREATED_DATETIME  TIMESTAMP(6) not null
);

create table USER (
    ID NUMBER (6) not null
    constraint USER_PK
    primary key,
    FIRST_NAME VARCHAR2(256) not null,
    LAST_NAME VARCHAR2(256) not null,
    USER_NAME VARCHAR2(256) not null unique,
    PASSWORD VARCHAR2(256) not null,
    ROLE role,
    IS_ENABLED boolean not null
    );

alter table FLOW_MONITORING_HISTORY
  add constraint FLOW_HISTORY_USER_FK
foreign key (USER_ID) references USER(ID);





