package com.offsidegaming.monitoringflow_rateservice.converter;

import com.offsidegaming.monitoringflow_rateservice.entity.FlowType;
import io.micrometer.core.instrument.util.StringUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class FlowEnumConverter implements Converter<String, FlowType> {
    @Override
    public FlowType convert(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        return FlowType.valueOf(value.toUpperCase());
    }
}