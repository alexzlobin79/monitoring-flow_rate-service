package com.offsidegaming.monitoringflow_rateservice.controller.swagger;

import com.offsidegaming.monitoringflow_rateservice.dto.request.RateFlowRequest;
import com.offsidegaming.monitoringflow_rateservice.dto.response.BaseResponse;
import com.offsidegaming.monitoringflow_rateservice.dto.response.FlowMonitoringHistoryDto;
import com.offsidegaming.monitoringflow_rateservice.dto.response.ResponseWithData;
import com.offsidegaming.monitoringflow_rateservice.entity.FlowType;
import com.offsidegaming.monitoringflow_rateservice.service.monitoring.IFlowMonitoringService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import java.time.LocalDate;
import java.util.Optional;

@RestController
@RequestMapping(
        value = "json/monitoring/",
        produces = "application/json;charset=UTF-8"
)
@Api(value = "monitoring-api", description = "Api Monitoring Flow Application")
@Slf4j
@AllArgsConstructor
public class AppController {

    @Autowired
    private final IFlowMonitoringService flowMonitoringService;

    @ApiOperation(
            value = "Retrieves history",
            notes = "requires USER rights",
            consumes = "text/plain",
            authorizations = {@Authorization(value = "basicAuth")}
    )
    @RequestMapping(value = "/history/{flowType}", method = RequestMethod.GET)
    public ResponseWithData<FlowMonitoringHistoryDto> getHistory(@ApiParam(example = "2019-10-29") @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                                         Optional<LocalDate> startDate,
                                                                 @ApiParam(example = "2020-10-05") @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                                         Optional<LocalDate> endDate,
                                                                 @ApiParam(example = "GAZ") @PathVariable FlowType flowType, Authentication authentication) {

        //reading all history

        if (!startDate.isPresent() && !endDate.isPresent()) {

            return flowMonitoringService.getAllHistory(authentication.getName(), flowType);

        }
          //reading  history from startDate(included)

        if (!endDate.isPresent()) {

            return flowMonitoringService.getHistoryAfter(authentication.getName(), flowType, startDate.get());

        }
        //reading  history to endDate(non included)
        if (!startDate.isPresent()) {

            return flowMonitoringService.getHistoryBefore(authentication.getName(), flowType, endDate.get());

        }
        //reading  history from startDate(included) to endDate (non included)
        return flowMonitoringService.getHistory(authentication.getName(), flowType, startDate.get(), endDate.get());

    }

    @ApiOperation(
            value = "Add rate flow",
            notes = "requires USER rights",
            consumes = "application/json;charset=UTF-8",
            authorizations = {@Authorization(value = "basicAuth")}
    )
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public BaseResponse addRateFlow(@Valid @RequestBody RateFlowRequest request, Authentication authentication) {
        return flowMonitoringService.addRateFlow(authentication.getName(), request);

    }

}
