package com.offsidegaming.monitoringflow_rateservice.mapper;

import com.offsidegaming.monitoringflow_rateservice.dto.request.RateFlowRequest;
import com.offsidegaming.monitoringflow_rateservice.dto.response.FlowMonitoringHistoryDto;
import com.offsidegaming.monitoringflow_rateservice.dto.response.RateFlowDto;
import com.offsidegaming.monitoringflow_rateservice.entity.FlowMonitoringHistoryEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MapperDto {

    @Autowired
    private ModelMapper modelMapper;

    public FlowMonitoringHistoryDto map(List<FlowMonitoringHistoryEntity> source) {

        FlowMonitoringHistoryDto dto = new FlowMonitoringHistoryDto();
        List<RateFlowDto> rates = source.stream()
                .map(e -> modelMapper.map(e, RateFlowDto.class))
                .collect(Collectors.toList());
        dto.setRates(rates);

        return dto;

    }

    public FlowMonitoringHistoryEntity map(RateFlowRequest source) {

        return modelMapper.map(source, FlowMonitoringHistoryEntity.class);

    }
}
