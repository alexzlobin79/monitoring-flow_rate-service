package com.offsidegaming.monitoringflow_rateservice.mapper;

public interface Mapper<S, D> {
    D map(S source);
}