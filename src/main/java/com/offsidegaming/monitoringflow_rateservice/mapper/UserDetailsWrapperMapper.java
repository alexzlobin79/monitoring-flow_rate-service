package com.offsidegaming.monitoringflow_rateservice.mapper;

import com.offsidegaming.monitoringflow_rateservice.entity.UserEntity;
import com.offsidegaming.monitoringflow_rateservice.service.wrapper.UserDetailWrapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class UserDetailsWrapperMapper implements Mapper<UserEntity, UserDetailWrapper> {

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    private void setupMapper() {

        modelMapper.typeMap(UserEntity.class, UserDetailWrapper.class)
                .addMapping(UserEntity::getUserName, UserDetailWrapper::setUsername);
    }

    @Override
    public UserDetailWrapper map(UserEntity source) {
        return modelMapper.map(source, UserDetailWrapper.class);
    }
}