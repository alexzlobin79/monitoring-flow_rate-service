package com.offsidegaming.monitoringflow_rateservice.service.monitoring;

import com.offsidegaming.monitoringflow_rateservice.dto.request.RateFlowRequest;
import com.offsidegaming.monitoringflow_rateservice.dto.response.BaseResponse;
import com.offsidegaming.monitoringflow_rateservice.dto.response.FlowMonitoringHistoryDto;
import com.offsidegaming.monitoringflow_rateservice.dto.response.ResponseWithData;
import com.offsidegaming.monitoringflow_rateservice.entity.FlowType;

import java.time.LocalDate;

public interface IFlowMonitoringService {

    ResponseWithData<FlowMonitoringHistoryDto> getAllHistory(String userName, FlowType flowType);

    ResponseWithData<FlowMonitoringHistoryDto> getHistory(String userName, FlowType flowType, LocalDate start, LocalDate end);

    ResponseWithData<FlowMonitoringHistoryDto> getHistoryAfter(String userName, FlowType flowType, LocalDate start);

    ResponseWithData<FlowMonitoringHistoryDto> getHistoryBefore(String userName, FlowType flowType, LocalDate end);

    BaseResponse addRateFlow( String login,RateFlowRequest request);


}
