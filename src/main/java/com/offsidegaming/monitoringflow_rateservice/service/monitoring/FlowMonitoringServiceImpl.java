package com.offsidegaming.monitoringflow_rateservice.service.monitoring;

import com.offsidegaming.monitoringflow_rateservice.dto.request.RateFlowRequest;
import com.offsidegaming.monitoringflow_rateservice.dto.response.BaseResponse;
import com.offsidegaming.monitoringflow_rateservice.dto.response.FlowMonitoringHistoryDto;
import com.offsidegaming.monitoringflow_rateservice.dto.response.ResponseWithData;
import com.offsidegaming.monitoringflow_rateservice.entity.FlowMonitoringHistoryEntity;
import com.offsidegaming.monitoringflow_rateservice.entity.FlowType;
import com.offsidegaming.monitoringflow_rateservice.entity.UserEntity;
import com.offsidegaming.monitoringflow_rateservice.mapper.MapperDto;
import com.offsidegaming.monitoringflow_rateservice.repository.FlowMonitoringHistoryRepository;
import com.offsidegaming.monitoringflow_rateservice.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class FlowMonitoringServiceImpl implements IFlowMonitoringService {

    private final FlowMonitoringHistoryRepository monitoringRepository;

    private final UserRepository userRepository;

    private final MapperDto mapper;

    @Override
    public ResponseWithData<FlowMonitoringHistoryDto> getAllHistory(String userName, FlowType flowType) {

        try {

            List<FlowMonitoringHistoryEntity> entities = monitoringRepository.findAllHistoryByUserNameAndFlow(userName, flowType.name());

            return new ResponseWithData<>(mapper.map(entities));

        } catch (Exception ex) {

            log.error("Error getting all history monitoring", ex);

            return new ResponseWithData<>("Error getting all history monitoring", ex);

        }

    }

    @Override
    public ResponseWithData<FlowMonitoringHistoryDto> getHistory(String userName, FlowType flowType, LocalDate start, LocalDate end) {
        try {

            List<FlowMonitoringHistoryEntity> entities = monitoringRepository.findHistoryByUserNameAndFlow(userName, flowType.name(), start, end);

            return new ResponseWithData<>(mapper.map(entities));

        } catch (Exception ex) {

            log.error("Error getting history monitoring", ex);

            return new ResponseWithData<>("Error getting history monitoring", ex);

        }
    }

    @Override
    public ResponseWithData<FlowMonitoringHistoryDto> getHistoryAfter(String userName, FlowType flowType, LocalDate start) {

        try {

            List<FlowMonitoringHistoryEntity> entities = monitoringRepository.findHistoryByUserNameAndFlowAfter(userName, flowType.name(), start);
            return new ResponseWithData<>(mapper.map(entities));

        } catch (Exception ex) {

            log.error("Error getting history monitoring", ex);

            return new ResponseWithData<>("Error getting history monitoring", ex);

        }
    }

    @Override
    public ResponseWithData<FlowMonitoringHistoryDto> getHistoryBefore(String userName, FlowType flowType, LocalDate end) {
        try {

            List<FlowMonitoringHistoryEntity> entities = monitoringRepository.findHistoryByUserNameAndFlowBefore(userName, flowType.name(), end);
            return new ResponseWithData<>(mapper.map(entities));

        } catch (Exception ex) {

            log.error("Error getting history monitoring", ex);

            return new ResponseWithData<>("Error getting history monitoring", ex);

        }

    }

    @Override
    public BaseResponse addRateFlow(String login, RateFlowRequest request) {

        try {

            UserEntity userEntity = userRepository.findByLogin(login);

            log.info("Found user by login:{} with  id:{}", login, userEntity.getId());

            FlowMonitoringHistoryEntity rateEntity = mapper.map(request);

            rateEntity.setUserId(userEntity.getId());

            monitoringRepository.saveAndFlush(rateEntity);

            return BaseResponse.success();

        } catch (Exception ex) {

            log.error("Error saving history monitoring", ex);

            return new BaseResponse("Error saving history monitoring", ex);

        }

    }
}
