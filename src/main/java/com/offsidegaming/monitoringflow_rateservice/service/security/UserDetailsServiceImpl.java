package com.offsidegaming.monitoringflow_rateservice.service.security;

import com.offsidegaming.monitoringflow_rateservice.entity.UserEntity;
import com.offsidegaming.monitoringflow_rateservice.repository.UserRepository;
import com.offsidegaming.monitoringflow_rateservice.mapper.Mapper;
import com.offsidegaming.monitoringflow_rateservice.service.wrapper.UserDetailWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Mapper<UserEntity, UserDetailWrapper> userDetailWrapperMapper;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByLogin(login);
        log.debug("loadUserByLogin by {} found {}", login, userEntity);

        if (userEntity == null) {
            throw new UsernameNotFoundException("Username " + login + " not found");
        }

        return userDetailWrapperMapper.map(userEntity);
    }
}
