package com.offsidegaming.monitoringflow_rateservice.repository;

import com.offsidegaming.monitoringflow_rateservice.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT u.* " +
                    "FROM USER u " +
                    "WHERE upper(trim(u.USER_NAME)) = upper(trim(:login))"
    )
    UserEntity findByLogin(@Param(value = "login") String login);

}