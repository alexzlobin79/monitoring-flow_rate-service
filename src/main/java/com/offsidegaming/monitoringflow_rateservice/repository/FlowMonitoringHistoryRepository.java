package com.offsidegaming.monitoringflow_rateservice.repository;

import com.offsidegaming.monitoringflow_rateservice.entity.FlowMonitoringHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface FlowMonitoringHistoryRepository extends JpaRepository<FlowMonitoringHistoryEntity, Integer> {

    @Query(nativeQuery = true,
            value = "SELECT m.* " +
                    "FROM FLOW_MONITORING_HISTORY m " +
                    "JOIN USER u ON u.ID=m.USER_ID " +
                    "WHERE upper(trim(u.USER_NAME)) = upper(trim(:userName))" +
                    "AND m.FLOW_TYPE=:flowType"
    )
    List<FlowMonitoringHistoryEntity> findAllHistoryByUserNameAndFlow(@Param(value = "userName") String userName, @Param(value = "flowType") String flowType);

    @Query(nativeQuery = true,
            value = "SELECT m.* " +
                    "FROM FLOW_MONITORING_HISTORY m " +
                    "JOIN USER u ON u.ID=m.USER_ID " +
                    "WHERE upper(trim(u.USER_NAME)) = upper(trim(:userName)) " +
                    "AND m.FLOW_TYPE=:flowType " +
                    "AND m.CREATED_DATETIME >= :start AND m.CREATED_DATETIME < :end"
    )
    List<FlowMonitoringHistoryEntity> findHistoryByUserNameAndFlow(@Param(value = "userName") String userName,
                                                                   @Param(value = "flowType") String flowType,
                                                                   @Param(value = "start") LocalDate start,
                                                                   @Param(value = "end") LocalDate end);

    @Query(nativeQuery = true,
            value = "SELECT m.* " +
                    "FROM FLOW_MONITORING_HISTORY m " +
                    "JOIN USER u ON u.ID=m.USER_ID " +
                    "WHERE upper(trim(u.USER_NAME)) = upper(trim(:userName)) " +
                    "AND m.FLOW_TYPE=:flowType " +
                    "AND m.CREATED_DATETIME >= :start "
    )
    List<FlowMonitoringHistoryEntity> findHistoryByUserNameAndFlowAfter(@Param(value = "userName") String userName,
                                                                        @Param(value = "flowType") String flowType,
                                                                        @Param(value = "start") LocalDate start
    );

    @Query(nativeQuery = true,
            value = "SELECT m.* " +
                    "FROM FLOW_MONITORING_HISTORY m " +
                    "JOIN USER u ON u.ID=m.USER_ID " +
                    "WHERE upper(trim(u.USER_NAME)) = upper(trim(:userName)) " +
                    "AND m.FLOW_TYPE=:flowType " +
                    "AND m.CREATED_DATETIME < :end "
    )
    List<FlowMonitoringHistoryEntity> findHistoryByUserNameAndFlowBefore(@Param(value = "userName") String userName,
                                                                         @Param(value = "flowType") String flowType,
                                                                         @Param(value = "end") LocalDate end
    );

}