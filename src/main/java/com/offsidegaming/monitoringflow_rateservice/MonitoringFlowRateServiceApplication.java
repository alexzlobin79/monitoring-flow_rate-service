package com.offsidegaming.monitoringflow_rateservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonitoringFlowRateServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitoringFlowRateServiceApplication.class, args);
	}

}
