package com.offsidegaming.monitoringflow_rateservice.conf;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@EnableSwagger2
public class Swagger2Config {

    @Bean
    public Docket api() {
        List<SecurityScheme> schemeList = new ArrayList<>();
        schemeList.add(new BasicAuth("basicAuth"));

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.offsidegaming.monitoringflow_rateservice.controller.swagger"))
                .build()
                .apiInfo(metaData())
                .securityContexts(Collections.singletonList(securityContext(schemeList)))
                .securitySchemes(schemeList)
                .ignoredParameterTypes(Authentication.class)
                .alternateTypeRules(alternateResponseTypeRules(new TypeResolver(), StreamingResponseBody.class, MultipartFile.class)
                        .toArray(AlternateTypeRule[]::new));
    }

    private Stream<AlternateTypeRule> alternateResponseTypeRules(TypeResolver typeResolver, Class<?> responseType, Class<?> alternateType) {
        ResolvedType resolvedResponseType = typeResolver.resolve(responseType);
        ResolvedType resolvedAlternateType = typeResolver.resolve(alternateType);
        return Stream.of(
                new AlternateTypeRule(resolvedResponseType, resolvedAlternateType),
                new AlternateTypeRule(typeResolver.resolve(ResponseEntity.class, resolvedResponseType), resolvedAlternateType));
    }

    private SecurityContext securityContext(List<SecurityScheme> schemes) {
        return SecurityContext.builder()
                .securityReferences(schemes.stream()
                        .map(scheme -> SecurityReference.builder()
                                .scopes(new AuthorizationScope[0])
                                .reference(scheme.getName())
                                .build())
                        .collect(Collectors.toList()))
                .build();
    }

    private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title("MONITORING FLOW API")
                .description("REST API for Monitoring Client")
                .termsOfServiceUrl("Terms of service")
                .version("1.0")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                .contact(new Contact("Alex Zlobin", "your url", "avzlobin79@gmail.com"))
                .build();
    }

}
