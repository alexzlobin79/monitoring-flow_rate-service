package com.offsidegaming.monitoringflow_rateservice.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "FLOW_MONITORING_HISTORY")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlowMonitoringHistoryEntity implements Serializable {

    @Id
    @SequenceGenerator(name = "FLOW_MONITORING_HISTORY_SEQ", sequenceName = "FLOW_MONITORING_HISTORY_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FLOW_MONITORING_HISTORY_SEQ")
    private Integer id;

    @Column(name = "USER_ID")
    @NotNull
    private Integer userId;

    @Column(name = "VALUE")
    @NotNull
    private Double value;

    @Column(name = "FLOW_TYPE")
    @Enumerated(EnumType.STRING)
    @NotNull
    private FlowType flowType;

    @Column(name = "CREATED_DATETIME")
    @CreationTimestamp
    private LocalDateTime createdDateTime;


}