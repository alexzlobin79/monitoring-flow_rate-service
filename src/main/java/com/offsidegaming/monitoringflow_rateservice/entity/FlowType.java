package com.offsidegaming.monitoringflow_rateservice.entity;

public enum FlowType {
    GAZ, HOT_WATER, COLD_WATER;

}
