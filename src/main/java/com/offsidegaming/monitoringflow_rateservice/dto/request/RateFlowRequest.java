package com.offsidegaming.monitoringflow_rateservice.dto.request;

import com.offsidegaming.monitoringflow_rateservice.entity.FlowType;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class RateFlowRequest {

    @NotNull
    @ApiModelProperty(example = "100.55",required = true)
    private Double value;

    @NotNull
    @ApiModelProperty(example = "GAZ",required = true)
    private FlowType flowType;

}
