package com.offsidegaming.monitoringflow_rateservice.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseResponse implements Serializable {

    @ApiModelProperty(required = true)
    protected boolean success;

    @ApiModelProperty(required = true,example = "there is text error")
    protected String errorMessage;

    public BaseResponse(String messageError, Exception ex) {

        this.success = false;
        this.errorMessage = String.format(messageError + ": %s", ex.getMessage());

    }

    public BaseResponse(String messageError) {

        this.success = false;
        this.errorMessage = messageError;

    }

    public static BaseResponse success() {

        return new BaseResponse(true, null);
    }

}