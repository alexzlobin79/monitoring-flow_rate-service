package com.offsidegaming.monitoringflow_rateservice.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.Valid;

@Data
@NoArgsConstructor
public class ResponseWithData<T> extends BaseResponse {
    @ApiModelProperty(required = true)
    private T data;

    public ResponseWithData(T data) {

        this.data = data;
        this.success = true;
        this.errorMessage = null;

    }

    public ResponseWithData(String errorMessage) {

        super(errorMessage);

    }

    public ResponseWithData(String errorMessage, Exception ex) {

        super(errorMessage, ex);

    }
}
