package com.offsidegaming.monitoringflow_rateservice.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.offsidegaming.monitoringflow_rateservice.entity.FlowType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "Rate Flow")
public class RateFlowDto {

    @ApiModelProperty(example = "10.512",required = true)
    private Double value;
    @ApiModelProperty(example = "GAZ",required = true)
    private FlowType flowType;
    @ApiModelProperty(example = "2020-01-31 11:35:24",required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdDateTime;

}