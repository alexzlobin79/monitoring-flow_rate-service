package com.offsidegaming.monitoringflow_rateservice.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class FlowMonitoringHistoryDto {
    @ApiModelProperty
    private List<RateFlowDto> rates;

}
