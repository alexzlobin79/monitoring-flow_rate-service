package com.offsidegaming.monitoringflow_rateservice;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Assert;

public class PasswordEncoderTest {

    private PasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Test
    void isDecodeTrue() {

        String password1 = "12345678";
        String password2 = "1234567";

        String encodeString1 = bCryptPasswordEncoder.encode(password1);
        String encodeString2 = bCryptPasswordEncoder.encode(password2);

        System.out.println("encode1:" + encodeString1 + " input:" + password1);

        System.out.println("encode2:" + encodeString2 + " input:" + password2);

        Assert.isTrue(bCryptPasswordEncoder.matches(password1, encodeString1), "decoding1 is not successful");
        Assert.isTrue(bCryptPasswordEncoder.matches(password2, encodeString2), "decoding2 is not successful");
    }

}
