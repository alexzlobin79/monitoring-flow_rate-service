package iTest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.offsidegaming.monitoringflow_rateservice.MonitoringFlowRateServiceApplication;
import com.offsidegaming.monitoringflow_rateservice.dto.request.RateFlowRequest;
import com.offsidegaming.monitoringflow_rateservice.dto.response.BaseResponse;
import com.offsidegaming.monitoringflow_rateservice.dto.response.FlowMonitoringHistoryDto;
import com.offsidegaming.monitoringflow_rateservice.dto.response.RateFlowDto;
import com.offsidegaming.monitoringflow_rateservice.dto.response.ResponseWithData;
import com.offsidegaming.monitoringflow_rateservice.entity.FlowMonitoringHistoryEntity;
import com.offsidegaming.monitoringflow_rateservice.entity.FlowType;
import com.offsidegaming.monitoringflow_rateservice.repository.FlowMonitoringHistoryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.Base64Utils;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MonitoringFlowRateServiceApplication.class)
@AutoConfigureMockMvc
public class AppControllerMockMvcTests {

    public static final ObjectMapper MAPPER;

    static {

        MAPPER = new ObjectMapper();
        //setup mapper for working with LocalDateTime
        MAPPER.registerModule(new JavaTimeModule());
        MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    }

    //set actual data for test
    private static final String LOGIN = "alex";

    private static final String PASSWORD = "12345678";

    private static final Integer USER_ID = 1;

    private static final Double VALUE = 1000.1;

    private static final FlowType FLOW_TYPE = FlowType.COLD_WATER;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private FlowMonitoringHistoryRepository flowMonitoringHistoryRepository;

    @Test
    public void testAddRateFlow() throws Exception {

        BaseResponse response = performPost("/json/monitoring/add", rateFlowRequest(), BaseResponse.class);

        Assert.assertTrue(response.isSuccess());
        Assert.assertNull( response.getErrorMessage());

        Example<FlowMonitoringHistoryEntity> example = Example.of(FlowMonitoringHistoryEntity.builder()
                .userId(USER_ID).value(VALUE).flowType(FLOW_TYPE).build());

        List<FlowMonitoringHistoryEntity> entities = flowMonitoringHistoryRepository.findAll(example);

        Assert.assertFalse(entities.isEmpty());

        FlowMonitoringHistoryEntity actualEntity = entities.get(0);

        Assert.assertEquals(FLOW_TYPE, actualEntity.getFlowType());
        Assert.assertEquals(VALUE, actualEntity.getValue());
        Assert.assertEquals(USER_ID, actualEntity.getUserId());

    }

    @Test
    public void testGetHistoryBefore() throws Exception {

        ResponseWithData<FlowMonitoringHistoryDto> response = performGet("/json/monitoring/history/gaz?endDate=2020-01-02",

                new TypeReference<ResponseWithData<FlowMonitoringHistoryDto>>() {
                });

        Assert.assertTrue(response.isSuccess());
        Assert.assertNull( response.getErrorMessage());
        List<RateFlowDto> rates = response.getData().getRates();
        Assert.assertFalse(rates.isEmpty());
        Assert.assertEquals(2, response.getData().getRates().size());

    }

    @Test
    public void testGetHistoryAfter() throws Exception {

        ResponseWithData<FlowMonitoringHistoryDto> response = performGet("/json/monitoring/history/gaz?startDate=2020-01-01",

                new TypeReference<ResponseWithData<FlowMonitoringHistoryDto>>() {
                });

        Assert.assertTrue(response.isSuccess());
        Assert.assertNull( response.getErrorMessage());
        List<RateFlowDto> rates = response.getData().getRates();
        Assert.assertFalse(rates.isEmpty());
        Assert.assertEquals(2, response.getData().getRates().size());

    }

    @Test
    public void testGetBetweenHistory() throws Exception {

        ResponseWithData<FlowMonitoringHistoryDto> response = performGet("/json/monitoring/history/gaz?startDate=2020-01-01&&endDate=2020-01-02",

                new TypeReference<ResponseWithData<FlowMonitoringHistoryDto>>() {
                });

        Assert.assertTrue(response.isSuccess());
        Assert.assertNull( response.getErrorMessage());
        List<RateFlowDto> rates = response.getData().getRates();
        Assert.assertFalse(rates.isEmpty());
        Assert.assertEquals(1, response.getData().getRates().size());

    }

    @Test
    public void testGetAllHistory() throws Exception {

        ResponseWithData<FlowMonitoringHistoryDto> response = performGet("/json/monitoring/history/gaz",

                new TypeReference<ResponseWithData<FlowMonitoringHistoryDto>>() {
                });

        Assert.assertTrue(response.isSuccess());
        Assert.assertNull(response.getErrorMessage());
        List<RateFlowDto> rates = response.getData().getRates();
        Assert.assertFalse(rates.isEmpty());
        Assert.assertEquals(3, response.getData().getRates().size());

    }

    private <T> T performGet(String url, TypeReference<T> responseTypeReference) throws Exception {
        MvcResult getResult = mockMvc
                .perform(
                        get(url)
                                .header("Authorization", getCredentials(LOGIN, PASSWORD))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(jsonPath("$.success", is(true)))
                .andReturn();
        return parseResponse(getResult, responseTypeReference);
    }

    private <T> T performPost(String url, Object request, Class<T> responseClass) throws Exception {
        String requestJson = asJsonString(request);
        MvcResult mvcResult = mockMvc
                .perform(
                        post(url)
                                .content(requestJson)
                                .header(HttpHeaders.AUTHORIZATION, getCredentials(LOGIN, PASSWORD))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)

                )
                .andExpect(jsonPath("$.success", is(true)))
                .andReturn();
        return parseResponse(mvcResult, responseClass);
    }

    public static String asJsonString(final Object obj) {
        try {
            final String jsonContent = MAPPER.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private <T> T parseResponse(MvcResult mvcResult, Class<T> valueType) throws IOException {
        String contentAsString = mvcResult.getResponse().getContentAsString();
        return MAPPER.readValue(contentAsString, valueType);
    }

    private <T> T parseResponse(MvcResult mvcResult, TypeReference<T> typeReference) throws IOException {
        String contentAsString = mvcResult.getResponse().getContentAsString();
        return MAPPER.readValue(contentAsString, typeReference);
    }

    private String getCredentials(String login, String password) {

        return "Basic " + Base64Utils.encodeToString((login + ":" + password).getBytes());
    }

    private RateFlowRequest rateFlowRequest() {

        return new RateFlowRequest(VALUE, FLOW_TYPE);

    }

}